extends Position2D

onready var parent = get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	update_pivot_angle();
	pass
	
func _physics_process(delta):
	update_pivot_angle()
	
func update_pivot_angle():
	if parent.motion.x + parent.motion.y != 0:
		rotation = parent.motion.angle()
	
