extends KinematicBody2D

var MAX_SPEED = 100
var ACCELERATION = 750
var motion = Vector2.ZERO

func _ready():
	$AnimatedSprite.flip_h = false
	$AnimatedSprite.play("idle")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func get_input_axis():
	var axis = Vector2.ZERO
	
	axis.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	axis.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	#axis.x = int(Input.is_action_pressed('ui_right')) - int(Input.is_action_pressed('ui_left'))
	#axis.y = int(Input.is_action_pressed('ui_down')) - int(Input.is_action_pressed('ui_up'))
	return axis.normalized()
	
func apply_friction(amount):
	if motion.length() > amount:
		motion -= motion.normalized() * amount
	else:
		motion = Vector2.ZERO
	
func apply_movement(acceleration):
	motion += acceleration
	motion = motion.clamped(MAX_SPEED)
	
func _physics_process(delta):
	var axis = get_input_axis()
	if axis == Vector2.ZERO:
		apply_friction(ACCELERATION * delta)
	else:
		apply_movement(axis * ACCELERATION * delta)
		
	motion = move_and_slide(motion)

	if ( motion.x + motion.y == 0 ):
		$AnimatedSprite.play("idle")
	else:
		$AnimatedSprite.play("walk")
	
	if ( motion.x < 0 ):
		$AnimatedSprite.flip_h = true
	if ( motion.x > 0 ):
		$AnimatedSprite.flip_h = false
